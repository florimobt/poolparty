#!/bin/bash

FILE=~/".oh-my-zsh/custom/custom.zsh";
DIR="/mnt/nfs/homes/$USER/Scripts/PoolParty";

#COLOR
RED="\033[38;5;1m";
GREEN="\033[38;5;40m";
YELLOW="\033[38;5;226m";
GREENBACK="\033[1;4;42m";
ORANGEBACK="\033[1;4;48;5;208m";
WHITE="\033[1;15m";
STOP="\033[0m";


echo -e "[=========================================================================================================================================]";
echo -e "\t\t\t\t\t______           _  ______          _         ";
echo -e "\t\t\t\t\t| ___ \         | | | ___ \        | |        ";
echo -e "\t\t\t\t\t| |_/ /__   ___ | | | |_/ /_ _ _ __| |_ _   _ ";
echo -e "\t\t\t\t\t|  __/ _ \ / _ \| | |  __/ _\` | '__| __| | | |";
echo -e "\t\t\t\t\t| | | (_) | (_) | | | | | (_| | |  | |_| |_| |";
echo -e "\t\t\t\t\t\_|  \___/ \___/|_| \_|  \__,_|_|   \__|\__, |";
echo -e "\t\t\t\t\t                                         __/ |";
echo -e "\t\t\t\t\t                                        |___/ ";
echo -e "[=========================================================================================================================================]";


printf "\t\t\t\t\t${WHITE}PoolParty est en cours d'instalation !${STOP}"
printf "\n\n\t\t\t\t${YELLOW}Tu as besoin de oh-my-zsh pour faire fonctionner PoolParty.${STOP}"
printf "\n\t\t\t      ${RED}___________________________________________________________________${STOP}"
printf "\n\n\t\t\t\t${YELLOW}Pour le telecharger veuillez executer la commande suivante:\n${STOP}"
printf "\t\t\t${GREEN}    sh -c \"\$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)\"${STOP}"
git clone https://gitlab.com/florimobt/poolparty.git /mnt/nfs/homes/$USER/Scripts/PoolParty
sleep 1
chmod 777 /mnt/nfs/homes/$USER/Scripts/PoolParty/poolparty.sh
echo "alias pp=$DIR/poolparty.sh">>$FILE;
echo "alias byepp=$DIR/byepp.sh">>$FILE;
printf "\n\n\n\n\n\n\t\t\t\t\t\t${GREEN}PoolParty a bien été installé !\n${STOP}"
printf "\t\t\t\t\t      ${RED}___________________________________${STOP}"
printf "\n\n\t\t\t\t${YELLOW}Pour l'utiliser tu dois te placer dans ${STOP}${WHITE}le fichier racine de ton module${STOP}.\n"
printf "\n\n\t\t\t\t    ${YELLOW}Il suffit ensuite de faire ${STOP}${WHITE}pp${STOP}\n"
chmod 777 /mnt/nfs/homes/$USER/Scripts/PoolParty/pp_help.sh
chmod 777 /mnt/nfs/homes/$USER/Scripts/PoolParty/msg.sh
chmod 777 /mnt/nfs/homes/$USER/Scripts/PoolParty/byepp.sh
printf "\t\t\t\t    ${GREEN}Si tu as besoin d'aide sur l'utilisation de PoolParty tu peux faire ${STOP}${WHITE}pphelp${STOP}\n"
echo "alias pphelp=$DIR/pp_help.sh">>$FILE;
printf "\t\t\t\t    ${GREEN}Tu peux redemarer ton terminal !\n\t\t\t\t    ${STOP}${WHITE}Bonne chance pour ta piscine :)${STOP}\n"\