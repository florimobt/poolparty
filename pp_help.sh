#!/bin/bash

#COLOR
RED="\033[38;5;1m";
GREEN="\033[38;5;40m";
YELLOW="\033[38;5;226m";
GREENBACK="\033[1;4;42m";
ORANGEBACK="\033[1;4;48;5;208m";
WHITE="\033[1;15m";
STOP="\033[0m";

echo -e "[=========================================================================================================================================]";
echo -e "\t\t\t\t\t______           _  ______          _         ";
echo -e "\t\t\t\t\t| ___ \         | | | ___ \        | |        ";
echo -e "\t\t\t\t\t| |_/ /__   ___ | | | |_/ /_ _ _ __| |_ _   _ ";
echo -e "\t\t\t\t\t|  __/ _ \ / _ \| | |  __/ _\` | '__| __| | | |";
echo -e "\t\t\t\t\t| | | (_) | (_) | | | | | (_| | |  | |_| |_| |";
echo -e "\t\t\t\t\t\_|  \___/ \___/|_| \_|  \__,_|_|   \__|\__, |";
echo -e "\t\t\t\t\t                                         __/ |";
echo -e "\t\t\t\t\t                                        |___/ ";
echo -e "[=========================================================================================================================================]";
printf "\n\n\n\n\n\n\t\t\t\t\t\t${GREEN}Bienvenue dans l'aide Pool Party\n${STOP}"
printf "\t\t\t\t\t      ${RED}___________________________________${STOP}"
printf "\n\n\t\t\t\t    ${YELLOW}Pour l'utiliser tu dois te placer dans ${STOP}${WHITE}le fichier racine de ton module${STOP}.\n"
printf "\t\t\t\t    ${WHITE}C'est a dire dans le dossier ou se trouve tout les autres dossier de ton module${STOP}\n"
printf "\t\t\t\t    ${YELLOW}Exemple pour le C00 -> le dossier ou se trouve le dossier ex00, ex01, ex02, ex03 etc..${STOP}\n"
printf "\n\t\t\t\t    ${GREEN}Il suffit maintenant de faire ${STOP}${WHITE}pp${STOP}\n"